module git.augendre.info/gaugendre/go-gpio-playground

go 1.17

require github.com/warthog618/gpiod v0.6.0

require golang.org/x/sys v0.0.0-20200223170610-d5e6a3e2c0ae // indirect
