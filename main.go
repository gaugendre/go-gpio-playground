package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/warthog618/gpiod"
	"github.com/warthog618/gpiod/device/rpi"
)

var ledState = 1
var ledLine *gpiod.Line
var ledMutex = &sync.Mutex{}

func main() {
	chip := setupGpio()
	defer chip.Close()
	defer cleanupLedLine(ledLine)
	startServer()
}

func setupGpio() (c *gpiod.Chip) {
	c, err := gpiod.NewChip("gpiochip0")
	if err != nil {
		panic(err)
	}

	led := rpi.J8p7
	ledLine, err = c.RequestLine(led, gpiod.AsOutput(ledState))
	if err != nil {
		panic(err)
	}
	return
}

func cleanupLedLine(line *gpiod.Line) {
	log.Println("Reverting pin to input")
	line.Reconfigure(gpiod.AsInput)
	line.Close()
}

func startServer() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", toggleLed)
	server := &http.Server{Addr: ":4000", Handler: mux}

	// Setting up signal capturing
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	log.Print("Server Started")

	<-stop
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		log.Fatalf("Error during shutdown: %+v", err)
	}
}

func toggleLed(w http.ResponseWriter, r *http.Request) {
	ledMutex.Lock()
	ledState ^= 1
	ledLine.SetValue(ledState)
	ledMutex.Unlock()
	log.Println("Toggle led")
	w.WriteHeader(200)
	fmt.Fprintln(w, "Toggling led")
}
